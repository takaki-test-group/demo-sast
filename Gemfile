source 'https://rubygems.org'
ruby "2.7.6"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

gem 'rails',      '6.0.1'
gem 'puma',       '3.12.2'
gem 'sass-rails', '5.1.0'
gem 'webpacker',  '4.0.7'
gem 'jbuilder',   '2.9.1'
gem 'bootsnap',   '1.13.0', require: false
gem 'devise'
gem 'rails-i18n'
gem 'devise-i18n'
gem 'devise-i18n-views'
gem 'paper_trail'
gem 'nested_form'
gem 'chartkick'
gem 'groupdate'
gem 'roo'
gem 'cancancan'
gem "aws-sdk-s3"
gem 'mini_magick', '~> 4.8'
gem 'active_storage_validations'
gem 'airbrake'
gem 'line-bot-api'
gem 'dotenv-rails'
gem 'newrelic_rpm'
gem 'redis'
gem 'sidekiq'
gem 'jp_prefecture'
gem 'turbo-rails'
gem 'stimulus-rails'
gem 'prawn'
gem 'prawn-table'
gem 'premailer-rails'
gem 'rack-user_agent'
gem 'pagy', '~> 5.10'
gem 'administrate'
gem 'unread'

group :development, :test do
  gem 'sqlite3', '1.4.1'
  gem 'byebug',  '11.0.1', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do 
  gem 'web-console',           '4.0.1'
  gem 'listen',                '3.1.5'
  gem 'spring',                '2.1.0'
  gem 'spring-watcher-listen', '2.0.1'
  gem 'bullet'
  # lint
  gem 'rubocop', require: false
  gem 'rubocop-performance', require: false
  gem 'rubocop-rails', require: false
  gem 'rubocop-rake', require: false
  gem 'rubocop-rubycw', require: false
end

group :test do
  gem 'capybara',           '3.28.0'
  gem 'selenium-webdriver', '3.142.4'
  gem 'webdrivers',         '4.1.2'
end

group :production do
  gem 'pg'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem "hotwire-rails", "~> 0.1.3"
